# You must have globally installed:

-Node version 10.10.0
-npm version 6.8.0

## Cloning the repo 

When cloning this repository on your computer you must run  **npm install** command to install the project dependencies.

## Running the project

Then, you need to run **npm start** command to start the development mode, automatically it will open a tab in the browser with the [http://localhost:3000](http://localhost:3000) , if not, yo can open a new tab and type directly [http://localhost:3000](http://localhost:3000) .The page will reload if you make edits. 

This project was made with Create React App. 

## Live Demo

For the other hand, if you want to skip the last steps you can check the following link to watch the live demo: [https://boring-bardeen-007f42.netlify.com/](https://boring-bardeen-007f42.netlify.com/)

------------------------------------------------------------------

# Debes tener instalado globalmente:

-Node versión 10.10.0
-npm versión 6.8.0

## Clonando el repo

Al clonar este repositorio en su computadora, debe ejecutar el comando **npm install** para instalar las dependencias del proyecto.

## Corriendo el poryecto

Luego, debe ejecutar el comando **npm start** para iniciar el modo de desarrollo, automáticamente abrirá una pestaña en el navegador con el localhost 3000; si no, puedo abrir una nueva pestaña y escribir directamente [http://localhost:3000](http://localhost:3000). La página se volverá a cargar si realiza ediciones.

Este proyecto se realizó con Create React App.

## Live Demo

Por otro lado, si desea omitir los pasos anteriores, puede consultar el siguiente enlace para ver la demostración en vivo:[https://boring-bardeen-007f42.netlify.com/](https://boring-bardeen-007f42.netlify.com/)


