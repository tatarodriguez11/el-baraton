import React from 'react'
import './confirmationForm.css'

export default (props)=>{
  return(
   
    <form action=" " onSubmit={props.onSubmit}>
      <input required className="confirmation-mail" placeholder="tucorreo@tupagina.com" type="email"/>
      <input type="submit" className="confirmation-button" value="Enviar" />
    </form>
    
  )
}