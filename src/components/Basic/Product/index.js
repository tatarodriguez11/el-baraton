import React from 'react'
import './product.css'
import {Tooltip , Icon} from 'evergreen-ui'

export default (props) => {
  const productAvailable = props.available ? "product-container" : "no-available"
  const contentAvailable = props.available ? "product-content" : "content-no-available"
  const alt = props.available ? "disponible" : "no disponible"
  const cart = props.available ? "cart-container":"no-cart"
  return (
    <div alt={alt} className={productAvailable} id={props.id} >
      <div className={cart}>
        <Tooltip content="Añadir al carrito">
          < button 
          className="add-cart-button"          
          onClick = {
           ()=> props.onClick(props.productName,props.price,props.quantity, props.id)
          }
          >
            < Icon icon = "shopping-cart" color="white" />
          </ button>

        </Tooltip>
      </div>
      <div alt={alt} className="product-image" style={{ backgroundImage: `url(http://mikemoir.com/mikemoir/wp-content/uploads/2015/06/MedRes_Product-presentation-2.jpg)` }} />
      <div className={contentAvailable}>
      <p className="product-name">{props.productName}</p>
      <p className="product-price">{props.price} </p>
      <p className="product-quantity">Cantidad: {props.quantity} </p>
        
      
      </div>
    </div>
  )
}