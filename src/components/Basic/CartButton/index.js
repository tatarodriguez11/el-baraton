import React from 'react'
import { Icon} from 'evergreen-ui'
import './cartButton.css'


export default (props)=>{
  return(
    
      <button className="cart-button" onClick={props.onClick}>
        < Icon icon = "shopping-cart"  color="white" />
      </button>
    
  )
}