import React from 'react'
import {IconButton, Tooltip} from 'evergreen-ui'
import './productCartCard.css'

  

  export default (props)=>{
    const price = parseInt(props.productPrice.replace('$', '').replace(',', ''))

    return(
      <div className="card-container" >
        <div className="delete-icon">
        <Tooltip content="Eliminar producto">
          < IconButton icon = "trash" appearance = "minimal" intent = "warning" onClick={()=>props.onClick(props.id, props.productName, props.productPrice, props.quantity)} />
        </Tooltip>
        </div>
        <div style={{display:"flex", flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
        <input type="number" className="card-input" 
        placeholder="cant"
        value={props.cartQuantity} 
        onChange={(event)=>props.onChange(props.id,event.target.value )} min={1} max={props.quantity} />
        <span className="card-name">{props.productName}</span>
        </div>
        <div style={{display:"flex", flexDirection:"column"}}>
        <span className="card-price">P.Unitario {props.productPrice}</span>
        <span className="card-total-price">P.Total $ {props.cartQuantity * price || 0}</span>
        </div>

      </div>
    )
  }


