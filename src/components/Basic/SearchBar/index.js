import React from 'react'
import './searchBar.css'

export default (props)=>{
  return(
    <input type="search" placeholder="Escribe el nombre del producto" className="search-input" onChange={props.onChange} value={props.value}/>
  )
}