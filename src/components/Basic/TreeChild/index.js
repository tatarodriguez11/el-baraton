import React,{useState} from 'react'
import {useSpring,animated} from 'react-spring'



  
  
  function TreeChild (props){
    const [open,setOpen]=useState(false)
    const style=useSpring(
      {height: open ? 'auto' : 0,flex:1,
      from: { height: 0,  overflow:"hidden", flex:0,transition:"flex 0.3s ease-out" }})
    return(
      < div style = {{
          width: "100%",
          textOverflow: "ellipsis"
        }}
          
         >
        <div
        open={open} 
        style = {{
          marginLeft: `${props.level * (props.marginLevel || 10)}px`,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
          // marginBottom: "5px"
        }}
        onClick={
          ()=>{

            setOpen(!open)
            props.onClick(props.category, props.level, props.products)
          }
        }
       
        >     
        

        <span> { props.children ? (open ? props.closeIcon : props.openIcon) : props.finalIcon} </span>
        {props.content}
       
        </div>
       
        
         <animated.div style={style}>

          {props.children}
          </animated.div>
           
          
        
        
      </div>
    )
  }



export default TreeChild