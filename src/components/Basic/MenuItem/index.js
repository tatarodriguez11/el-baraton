import React from 'react'
import './menuItem.css'

export default (props) => {
  const selected = props.selected ? "selected" : "normal"
  return <span onClick={props.onClick} id={props.id} className={selected}>
    {props.children}
  </span>
}

