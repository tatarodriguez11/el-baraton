import React from 'react'
import './filterButton.css'


class FilterButton extends React.Component{
  render(){
    return(
      < div className = "dropdown" >
        <button className="dropbtn">{this.props.title}</button>
        <button className="dropbtn-small-device">{this.props.titleSmall}</button>
        < div className = "dropdown-content" >
          {this.props.list.map((item,index)=>{
            return <span key={index} onClick={()=>this.props.onClick(item)}>{item.text}</span>
          })}
        </div>

      </div>
    )
  }
}


export default FilterButton