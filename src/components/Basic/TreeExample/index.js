import React from 'react'
import TreeChild from '../../Basic/TreeChild'


class TreeExample extends React.Component{

  defaultContent = (category, level, data)=>{
    return <span>{category.name}</span>
  }

  //data must be an array
  treeFunction=(data,level=1)=>{
    
   return data.map((category)=>{
      return(
        <TreeChild 
        key={category.id}
        closeIcon={this.props.closeIcon}
        openIcon={this.props.openIcon}
        finalIcon={this.props.finalIcon}
        content = {
          this.props.content ? this.props.content(category, level, data) : this.defaultContent
        }
        level={level}
        products={this.props.products}
        onClick = {
          this.props.onClick
        }
        category={category}

        >
          {category.sublevels && this.treeFunction(category.sublevels,level+1)}
        </TreeChild>
      )
    })
   
  }

  render(){
    return (
      <div>

      {this.treeFunction(this.props.data)}
      </div>
      )
           
  }
}

export default TreeExample