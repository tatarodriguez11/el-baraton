import React from 'react'
import Tree from '../../Basic/TreeExample'
import data from '../../../data/categories.json'
import MenuItem from '../../Basic/MenuItem'
import {Icon, SideSheet, Position} from 'evergreen-ui'
import './sideMenu.css'


class SideMenu extends React.Component{


  state={
    menuShow:false
  }

  menuIconClick=()=>{
    this.setState({
      menuShow:true
    })
  }



  render(){
    return(
      <div className="menu-container">
        <div className="menu-content-tree">
          <h2>Menú</h2>
        <Tree 
          data = { data.categories }     
          content = {(category) => <MenuItem id={category.id} selected={this.state.itemSelected} >{category.name}</MenuItem>}
          closeIcon = { <Icon icon = "delete" color = "gray" marginRight = "5px" /> }
          openIcon = {<Icon icon = "add" color = "gray" marginRight = "5px" /> }
          finalIcon = { <Icon icon = "symbol-circle" color = "gray" marginRight = "5px" />} 
          products={this.props.products}
          onClick={this.props.onClick}        
          />
        </div>
        <div className="menu-content-small-device">
            <button className="menu-button" onClick={this.menuIconClick}>
              <Icon icon="menu" color="white" />
            </button>
       </div>
       <SideSheet
         isShown={this.state.menuShow}
         width={200}
         position={Position.LEFT}
         onCloseComplete = { () => this.setState({menuShow:false}) } 
         containerProps = {{
             display: 'flex',
             flex: '1',
             flexDirection: 'column',
             paddingTop:"30px"
           }}
       >
          <Tree 
          data = { data.categories }     
          content = {(category) => <MenuItem id={category.id} selected={this.state.itemSelected} >{category.name}</MenuItem>}
          closeIcon = { <Icon icon = "delete" color = "gray" marginRight = "5px" /> }
          openIcon = {<Icon icon = "add" color = "gray" marginRight = "5px" /> }
          finalIcon = { <Icon icon = "symbol-circle" color = "gray" marginRight = "5px" />} 
          products={this.props.products}
          onClick={this.props.onClick}        
          />
       </SideSheet>
      </div>
    )
  }
}


export default SideMenu