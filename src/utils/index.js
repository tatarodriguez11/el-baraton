export const removeByIndex = (array, index)=>{
  const newArray = [...array]
  newArray.splice(index, 1)
  return newArray

}


export const addPropertyToObject = (arr, index,property, value)=>{

  const newArr = [...arr]
  newArr[index][property] = value
  return newArr
}