export function setProducts (cartProducts){
  localStorage.setItem('products', JSON.stringify(cartProducts))
}

export function getProducts (){
 return JSON.parse(localStorage.getItem('products'))
}

export function removeProducts(){
  localStorage.removeItem('products')
}