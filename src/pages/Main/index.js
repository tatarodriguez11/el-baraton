import React, {Fragment} from 'react'
import products from '../../data/products.json'
import NavBar from '../../components/Basic/NavBar'
import './main.css'
import Product from '../../components/Basic/Product'
import {SideSheet, toaster, Dialog, Icon} from 'evergreen-ui'
import SearchInput from '../../components/Basic/SearchBar'
import FilterButton from '../../components/Basic/FilterButton'
import {AVAILABILITY_LIST , PRICE_LIST, QUANTITY_LIST, AVAILABILITY_SORT,PRICE_SORT, QUANTITY_SORT} from '../../data/constants'
import CartButton from '../../components/Basic/CartButton'
import SideMenu from '../../components/Compund/SideMenu'
import ProductCartCard from '../../components/Basic/ProductCartCard'
import ConfirmationForm from '../../components/Basic/ConfirmationForm'
import {setProducts, getProducts, removeProducts} from '../../storage'
import {removeByIndex , addPropertyToObject} from '../../utils'




class Main extends React.Component {

  state = {    
    itemSelected:false,
    searchValue:"",
    products:[],
    filteredProducts:[],
    message:"",
    priceSelected:"",
    cartOpen:false,
    cartProducts:[],
    dialogShow: false,
    cartMessage: "No has añadido productos al carrito",
    cartQuantity:1
  }  

  onTreeChange = (category,level,products) => {
    
    
    if(level>1){
      const  filteredProducts = products.filter((product) => product.sublevel_id === category.id)
      this.setState({
        filteredProducts : filteredProducts || [],
        categoryName:category.name,
        products : filteredProducts || []
        // itemSelected: !this.state.itemSelected
      })
    }else if(level===1){
      this.setState({
        filteredProducts:[],
        categoryName:"",
        products:[]
      })
    }
    
  }

  search=(event)=>{
    const searchedProduct = event.target.value
    this.setState({
      searchValue : searchedProduct
    })

    const newFilteredProducts = this.state.products.filter((product)=>{
      return(product.name.toLowerCase().includes(searchedProduct.toLowerCase()))

    })
    if(newFilteredProducts.length){

      this.setState({
        filteredProducts : newFilteredProducts
      })
    }else{
      this.setState({
        message:"Lo sentimos, no encontramos lo que buscabas",
        filteredProducts:[]
      })
    }
    
  }

  availabilitySelected=(item)=>{
    if(item.availability !== null){

      const newFilteredProducts = this.state.products.filter((product)=>{
        return(
          product.available === item.availability
          )
        })
        newFilteredProducts.length ? 
        this.setState({
          filteredProducts : newFilteredProducts
        })
        :
        this.setState({
        message:"Lo sentimos, no encontramos lo que buscabas",
        filteredProducts:[]
        })

    }else{
      this.setState({
        filteredProducts : this.state.products
      })
    }
  }

  priceSelected=(item)=>{  

    if(item.value !== null){
      const newFilteredProducts = this.state.products.filter((product)=>{
        const price = product.price.replace('$','').replace(',','')
        const productPrice = parseInt(price)
        return(
          productPrice > item.value[0] && productPrice < item.value[1]
        )
      })
      newFilteredProducts.length > 0 ?
        this.setState({
          filteredProducts: newFilteredProducts
        })
        :
        this.setState({
          message: "Lo sentimos, no encontramos lo que buscabas",
          filteredProducts: []
        })
    }else{
      this.setState({
        filteredProducts : this.state.products
      })
    }
  }

  priceSlider=(value)=>{
    this.setState({
      priceSelected : value
    })

    console.log(this.state.priceSelected)
  }
  
  quantitySelected=(item)=>{
    if(item.value !== null){
      const newFilteredProducts = this.state.products.filter((product)=>{
        return(
          product.quantity > item.value[0] && product.quantity < item.value[1]
        )
      })
      newFilteredProducts.length > 0 ?
        this.setState({
          filteredProducts: newFilteredProducts
        }) :
        this.setState({
          message: "Lo sentimos, no encontramos lo que buscabas",
          filteredProducts: []
        })
    } else {
      this.setState({
        filteredProducts: this.state.products
      })
    }
  }

  availabilitySort=(item)=>{
    
    const products =[...this.state.filteredProducts]
   
    if(item.value===1){
      products.sort(
        function (x, y) {
          return y.available - x.available
        }
      )
      this.setState({
        filteredProducts : products
      })
    }else{
      products.sort(
        function (x, y) {
          return x.available - y.available
        }
      )
      this.setState({
        filteredProducts: products
      })

    }
  }

  priceSort=(item)=>{
    const products = [...this.state.filteredProducts]
    
      if(item.value === 1){
        //ordena de menor a mayor precio
        products.sort(
          function (a, b) {
            return parseInt(a.price.replace('$', '').replace(',', '')) - parseFloat(b.price.replace('$', '').replace(',', ''));
          }
        )
        this.setState({
          filteredProducts : products
        })
      }else{
        //ordena de mayor a menor precio
        products.sort(
          function (a, b) {
            return parseInt(b.price.replace('$', '').replace(',', '')) - parseFloat(a.price.replace('$', '').replace(',', ''));
          }
        )
         this.setState({
           filteredProducts: products
         })
      }

  }

  quantitySort=(item)=>{
    const products = [...this.state.filteredProducts]
    if(item.value === 1){
      products.sort(
        (a, b) => a.quantity - b.quantity
      )

      this.setState({
        filteredProducts : products
      })
    }else{
      products.sort(
        (a,b)=> b.quantity - a.quantity
      )
      this.setState({
        filteredProducts : products
      })
    }

  }

  

  addProductCart = (productName,price,quantity, id) => {
    const product ={ name : productName, price, quantity, id}

    if (this.state.cartProducts.find(product => product.id === id) === undefined){
     
      this.setState({
        cartProducts : [...this.state.cartProducts , product]
      })
      setProducts([...this.state.cartProducts, product])


      toaster.success('Producto añadido')
    }else{
      toaster.warning('Este producto ya está en el carrito', {
        description: 'Por favor revisa el carrito de compras',
        duration : 10
      })
    }

    

    
  }

  cartClick=()=>{
    this.setState({
      cartOpen : true
    })
  }

 removeCartItem = (id) => { 
   
   const indice = this.state.cartProducts.findIndex(product=>product.id === id)

    const newProducts = removeByIndex(this.state.cartProducts , indice)

     this.setState({
       cartProducts : newProducts
     })

       setProducts([...newProducts])

 }

 onCartItemChange=(id, value)=>{
  
   const indice = this.state.cartProducts.findIndex(product => product.id === id)
    const property = 'cartQuantity'
   const newProducts = addPropertyToObject(this.state.cartProducts, indice, property, value)

    this.setState({
      cartProducts: newProducts
    })
    
    setProducts([...this.state.cartProducts])

 }

 componentDidMount(){
   const cartProducts = getProducts()
   this.setState({
     cartProducts : cartProducts || []
   })
 }


  render() {

    return ( 
      <Fragment >
      <  NavBar />
      <CartButton onClick={this.cartClick} />
      <SideSheet
        isShown={this.state.cartOpen}
        width={270}
        onCloseComplete={()=>this.setState({cartOpen:false})}
        containerProps = {{
             display: 'flex',
             flex: '1',
             flexDirection: 'column',
             paddingTop:"30px",
             paddingLeft:"10px",
             paddingRight:"10px"
           }}
        shouldCloseOnEscapePress={false}
      >
        <h3>Carrito de compras</h3>

        {
          this.state.cartProducts.length ? 
          this.state.cartProducts.map((product)=>{
            return(
              <ProductCartCard 
                productName={product.name}
                productPrice={product.price}
                quantity={product.quantity}
                key={product.id}
                onClick={this.removeCartItem}
                id={product.id}
                onChange={this.onCartItemChange}
                cartQuantity={product.cartQuantity}
              />

            )
          })
          :
          <p>{this.state.cartMessage}</p>
        }

       

        {
          !!this.state.cartProducts.length && 
          < button 
          onClick = {
            () => this.setState({
              dialogShow : true
            })
          }
          className = "cart-shop-button" > Ir a pagar </button>
        }
        <Dialog
        isShown={this.state.dialogShow}
        
        onCloseComplete={()=>this.setState({dialogShow: false})}
        hasFooter={false}
        hasHeader={false}
        shouldCloseOnEscapePress={false}
        >   
        < h2 > Gracias por usar nuestra web </h2>       
          Déjanos tu correo para enviar el link de pago
          <ConfirmationForm  
          onSubmit={(event)=>{
           event.preventDefault()
           removeProducts()
          this.setState({
            dialogShow:false,
            cartProducts:[],
            cartMessage:"Hemos enviado tu cuenta al correo que nos diste"
          })} 
          }
          />

        </Dialog>

      </SideSheet>

       <div className = "general-wrapper" >
         <SideMenu 
          onClick={this.onTreeChange}
          products={products.products}
         />
        <div className = "products-container" >
            <SearchInput onChange={this.search} value={this.state.value}/>
              
            <div className="filter-section">
              <span>Filtros:</span>
              <div className="filter-buttons" >              
              < FilterButton 
              title = "Disponibilidad"
              onClick = { this.availabilitySelected }
              list = {AVAILABILITY_LIST }
              titleSmall={<Icon icon="small-tick" />}
              />              
              < FilterButton title = "Precio"
              onClick={this.priceSelected}
              list = {PRICE_LIST  }
              titleSmall={<Icon icon="dollar" />}

              />
              < FilterButton title = "Cantidad"
              onClick={this.quantitySelected}
              list = {QUANTITY_LIST }
              titleSmall={<Icon icon="multi-select" />}

              />
              </div>
            </div>
             <div className="filter-section">
              <span>Ordenar:</span>
              <div className="sort-buttons">
              < FilterButton title = "Disponibilidad"
              onClick = { this.availabilitySort }
              list = {AVAILABILITY_SORT }
              titleSmall={<Icon icon="small-tick" />}

              />
              < FilterButton title = "Precio"
              onClick={this.priceSort}
              list = {PRICE_SORT  }
              titleSmall={<Icon icon="dollar" />}

              />
              < FilterButton title = "Cantidad"
              onClick={this.quantitySort}
              list = {QUANTITY_SORT }
              titleSmall={<Icon icon="multi-select" />}

              />
              </div>
            </div>
                <h2>{this.state.categoryName}</h2>
                <div className = "products" > 
               
                { this.state.filteredProducts.length > 0 ?
                  this.state.filteredProducts.map((product) => {

                    return (

                      <Product
                      productName = {  product.name }
                      price = {  product.price    }
                      key = { product.id  }
                      available={product.available}
                      quantity={product.quantity}
                      onClick={this.addProductCart}
                      id={product.id}
                      
                      />
                    )
                  })
                  :
                  <p>{this.state.message}</p>
                 
                } 
                </div> 
        </div> 
      </div> 
      </Fragment>
    )
  }
}

export default Main