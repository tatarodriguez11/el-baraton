export const AVAILABILITY_LIST = [{
  text: "Disponible",
  availability: true
}, {
  text: "No Disponible",
  availability: false
}, {
  text: "Todos",
  availability: null
}]

export const AVAILABILITY_SORT = [{
  text: "Disponibles primero",
  value: 1
}, {
  text: "No disponibles primero",
  value: 2
}]



export const PRICE_LIST = [{
    text: "menos de $10.000",
    value: [0, 10000]
  }, {
    text: "$10.000 - $20.000",
    value: [10000, 20000]
  }, {
    text: "más de $20.000",
    value: [20000, 99999999999]
  },
  {
    text: "todos",
    value: null
  }
]

export const PRICE_SORT=[
  {
    text:"menor a mayor",
    value:1
  },
  {
    text:"mayor a menor",
    value:2
  }
]

export const QUANTITY_LIST = [{
    text: "menos de 100",
    value: [0, 100]
  },
  {
    text: "100 - 300",
    value: [100, 300]
  },
  {
    text: "300 - 500",
    value: [300, 500]
  },
  {
    text: "500 - 700",
    value: [500, 700]
  },
  {
    text: "700 - 900",
    value: [700, 900]
  },
  {
    text: "más de 900",
    value: [900, 99999999999999]
  },
  {
    text: "todos",
    value: null
  }
]

export const QUANTITY_SORT=[
  {
    text:"menor a mayor",
    value:1
  },
  {
    text:"mayor a menor",
    value:2
  }
]